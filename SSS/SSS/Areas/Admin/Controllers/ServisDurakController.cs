﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SSS.Areas.Admin.Models;
using SSS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSS.Areas.Admin.Controllers
{
    public class ServisDurakController : AdminController
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/ServisDurak
        public ActionResult Index()
        {
            
            
            return View();
        }
        public ActionResult ServisDurak()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ServisDurak(OgrenciDurakEkle OgrDurak)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(OgrDurak.OgrenciAd);
            var userId = kullanici.Ogrenci_Id;
            var ogr = context.Ogrencis.Find(userId);
            var durak = new Durak { Ilce = OgrDurak.Ilce, Mahalle = OgrDurak.Mahalle, Sokak = OgrDurak.Sokak, Latitude = OgrDurak.Latitude, Longitude = OgrDurak.Longitude };
            if (ogr.Durak_Id==null)
            {
                context.Duraks.Add(durak);
            }
            ogr.Durak_Id = durak.Id;
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}