﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SSS.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SSS.Areas.Admin.Models;

namespace SSS.Areas.Admin.Controllers
{
    public class SoforsController : AdminController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public SoforsController(){

        }

        public SoforsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Admin/Sofors
        public ActionResult Index()
        {
            return View(db.Sofor.ToList());
        }

        // GET: Admin/Sofors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sofor sofor = db.Sofor.Find(id);
            if (sofor == null)
            {
                return HttpNotFound();
            }
            return View(sofor);
        }

        // GET: Admin/Sofors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Sofors/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Ad,Soyad")] Sofor sofor,SoforEkleModel model)
        {
            if (ModelState.IsValid)
            {
                var sofr = new Sofor { Ad = model.Ad, Soyad = model.Soyad };
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email,PhoneNumber=model.Telefon,Sofor_Id=sofr.Id};
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    db.Sofor.Add(sofor);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                AddErrors(result);
            }
            return View(model);
        }

        // GET: Admin/Sofors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sofor sofor = db.Sofor.Find(id);
            if (sofor == null)
            {
                return HttpNotFound();
            }
            return View(sofor);
        }

        // POST: Admin/Sofors/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Soyad")] Sofor sofor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sofor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sofor);
        }

        // GET: Admin/Sofors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sofor sofor = db.Sofor.Find(id);
            if (sofor == null)
            {
                return HttpNotFound();
            }
            return View(sofor);
        }

        // POST: Admin/Sofors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sofor sofor = db.Sofor.Find(id);
            db.Sofor.Remove(sofor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
