﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SSS.Areas.Admin.Models;
using SSS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSS.Areas.Admin.Controllers
{
    public class RolYonetimiController : AdminController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetimi
        public ActionResult Index()
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(rolStore);
            
            rolManager.Create(new IdentityRole("Ogrenci"));
            rolManager.Create(new IdentityRole("Sofor"));
            rolManager.Create(new IdentityRole("Yonetici"));


            var model = rolManager.Roles.ToList();

            return View(model);
        }

        public ActionResult RolEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolEkle(RolEkleModel rol)
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(rolStore);
            if (rolManager.RoleExists(rol.RolAd)==false)
            {
                rolManager.Create(new IdentityRole(rol.RolAd));
            }

            return RedirectToAction("Index");
        }

        public ActionResult RolKullaniciEkle()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult RolKullaniciEkle(KullaniciRolModel model)
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(rolStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.KullaniciAdi);
            if (!userManager.IsInRole(kullanici.Id, model.RolAdi))
            {
                userManager.AddToRole(kullanici.Id, model.RolAdi);
            }
            return RedirectToAction("Index");
        }

    }
}