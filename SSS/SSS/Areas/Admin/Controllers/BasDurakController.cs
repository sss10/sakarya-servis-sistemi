﻿using SSS.Areas.Admin.Models;
using SSS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSS.Areas.Admin.Controllers
{
    public class BasDurakController : AdminController
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Admin/BasDurak
        public ActionResult Index()
        {
            

            return View();
        }
        public ActionResult BaslangicDurakEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BaslangicDurakEkle(DurakBaslangic bs)
        {
            var durak = db.Duraks.Find(14);
            durak.Latitude = bs.BasLan;
            durak.Longitude = bs.BasLon;
            db.SaveChanges();
            return Redirect("/Ogrencis/Index");
        }
        public ActionResult BitisDurakEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult BitisDurakEkle(DurakBaslangic bs)
        {
            var durak = db.Duraks.Find(15);
            durak.Latitude = bs.BasLan;
            durak.Longitude = bs.BasLon;
            db.SaveChanges();
            return Redirect("/Ogrencis/Index");
        }
    }
}