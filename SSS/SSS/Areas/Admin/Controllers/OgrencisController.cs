﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SSS.Models;
using Microsoft.AspNet.Identity;

namespace SSS.Areas.Admin.Controllers
{
    public class OgrencisController : AdminController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Ogrencis
        public ActionResult Index()
        {
            var ogrencis = db.Ogrencis.Include(o => o.Durak).Include(o => o.Okul);
            return View(ogrencis.ToList());
        }

        // GET: Admin/Ogrencis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogrenci ogrenci = db.Ogrencis.Find(id);
            if (ogrenci == null)
            {
                return HttpNotFound();
            }
            return View(ogrenci);
        }

        // GET: Admin/Ogrencis/Create
        public ActionResult Create()
        {
            
            ViewBag.Okul_Id = new SelectList(db.Okuls, "Id", "OkulAdi");
            return View();
        }

        // POST: Admin/Ogrencis/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Soyad,Telefon,Okul_Id,Durak_Id")] Ogrenci ogrenci)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var user = db.Users.Find(userId);
                db.Ogrencis.Add(ogrenci);
                user.Ogrenci_Id = ogrenci.Id;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            
            ViewBag.Okul_Id = new SelectList(db.Okuls, "Id", "OkulAdi", ogrenci.Okul_Id);
            return View(ogrenci);
        }

        // GET: Admin/Ogrencis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogrenci ogrenci = db.Ogrencis.Find(id);
            if (ogrenci == null)
            {
                return HttpNotFound();
            }
            ViewBag.Durak_Id = new SelectList(db.Duraks, "Id", "Ilce", ogrenci.Durak_Id);
            ViewBag.Okul_Id = new SelectList(db.Okuls, "Id", "OkulAdi", ogrenci.Okul_Id);
            return View(ogrenci);
        }

        // POST: Admin/Ogrencis/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Soyad,Telefon,Okul_Id,Durak_Id")] Ogrenci ogrenci)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ogrenci).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Durak_Id = new SelectList(db.Duraks, "Id", "Ilce", ogrenci.Durak_Id);
            ViewBag.Okul_Id = new SelectList(db.Okuls, "Id", "OkulAdi", ogrenci.Okul_Id);
            return View(ogrenci);
        }

        // GET: Admin/Ogrencis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogrenci ogrenci = db.Ogrencis.Find(id);
            if (ogrenci == null)
            {
                return HttpNotFound();
            }
            return View(ogrenci);
        }

        // POST: Admin/Ogrencis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ogrenci ogrenci = db.Ogrencis.Find(id);
            db.Ogrencis.Remove(ogrenci);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
