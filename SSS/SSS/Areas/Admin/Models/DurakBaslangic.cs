﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Areas.Admin.Models
{
    public class DurakBaslangic
    {
        public double BasLan { get; set; }
        public double BasLon { get; set; }
        public double SonLan { get; set; }
        public double SonLon { get; set; }
    }
}