﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Areas.Admin.Models
{
    public class KullaniciRolModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}