﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSS.Areas.Admin.Models
{
    public class SoforEkleModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} harften oluşmalı.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Ad")]
        public string Ad { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} harften oluşmalı.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Soyad")]
        public string Soyad { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} rakamdan oluşmalı.", MinimumLength = 10)]
        [DataType(DataType.Text)]
        [Display(Name = "Telefon")]
        public string Telefon { get; set; }
        
        [Required]
        [EmailAddress]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} uzunluğunda olmalı.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Sifre")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Tekrarlayiniz")]
        [Compare("Password", ErrorMessage = "Girilen sifreler uyusmamaktadir.")]
        public string ConfirmPassword { get; set; }
    }
}