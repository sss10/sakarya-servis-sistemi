﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Areas.Admin.Models
{
    public class OgrenciDurakEkle
    {
        public string OgrenciAd { get; set; }
        public string Ilce { get; set; }
        public string Mahalle { get; set; }
        public string Sokak { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}