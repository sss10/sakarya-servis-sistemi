﻿using System.Web.Mvc;

namespace SSS.Areas.UyeSofor
{
    public class UyeSoforAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UyeSofor";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "UyeSofor_default",
                "UyeSofor/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}