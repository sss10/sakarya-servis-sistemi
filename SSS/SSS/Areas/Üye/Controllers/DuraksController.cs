﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SSS.Models;
using Microsoft.AspNet.Identity;

namespace SSS.Areas.Üye.Controllers
{
    
    public class DuraksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Üye/Duraks
        public ActionResult Index()
        {
            return View(db.Duraks.ToList());
        }

        // GET: Üye/Duraks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Durak durak = db.Duraks.Find(id);
            if (durak == null)
            {
                return HttpNotFound();
            }
            return View(durak);
        }

        // GET: Üye/Duraks/Create
        //[Authorize]
        public ActionResult DurakBilgisi()
        {
            return View();
        }

        // POST: Üye/Duraks/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DurakBilgisi([Bind(Include = "Id,Ilce,Mahalle,Sokak,Latitude,Longitude")] Durak durak)
        {
            
            if (ModelState.IsValid)
            {
                
                var userId = User.Identity.GetUserId();
                var user = db.Users.Find(userId);
                var ogrId = user.Ogrenci_Id;
                var ogr = db.Ogrencis.Find(ogrId);
                if (ogr.Durak_Id==null)
                {
                    db.Duraks.Add(durak);
                }
                ogr.Durak_Id = durak.Id;
                
                db.SaveChanges();
                return Redirect("/Üye/Gunservis/Index");
            }

            return View(durak);
        }

        // GET: Üye/Duraks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Durak durak = db.Duraks.Find(id);
            if (durak == null)
            {
                return HttpNotFound();
            }
            return View(durak);
        }

        // POST: Üye/Duraks/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ilce,Mahalle,Sokak,Latitude,Longitude")] Durak durak)
        {
            if (ModelState.IsValid)
            {
                db.Entry(durak).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(durak);
        }

        // GET: Üye/Duraks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Durak durak = db.Duraks.Find(id);
            if (durak == null)
            {
                return HttpNotFound();
            }
            return View(durak);
        }

        // POST: Üye/Duraks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Durak durak = db.Duraks.Find(id);
            db.Duraks.Remove(durak);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
