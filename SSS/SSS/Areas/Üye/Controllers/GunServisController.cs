﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SSS.Models;

namespace SSS.Areas.Üye.Controllers
{
    
    public class GunServisController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Üye/GunServis
        public ActionResult Index()
        {
            var gunlerServis = db.GunlerServis.Include(g => g.Gunler).Include(g => g.Servis);
            ViewBag.Servis_Id = new SelectList(db.Servis, "Id", "Saat");
            
            db.SaveChanges();
            return View(gunlerServis.ToList());
        }

        // GET: Üye/GunServis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GunServis gunServis = db.GunlerServis.Find(id);
            if (gunServis == null)
            {
                return HttpNotFound();
            }
            return View(gunServis);
        }

        // GET: Üye/GunServis/Create
        public ActionResult Create()
        {
            ViewBag.Gunler_Id = new SelectList(db.Gunler, "Id", "GunAd");
            ViewBag.Servis_Id = new SelectList(db.Servis, "Id", "Saat");
            return View();
        }

        // POST: Üye/GunServis/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Gunler_Id,Servis_Id")] GunServis gunServis)
        {
            if (ModelState.IsValid)
            {
                db.GunlerServis.Add(gunServis);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Gunler_Id = new SelectList(db.Gunler, "Id", "GunAd", gunServis.Gunler_Id);
            ViewBag.Servis_Id = new SelectList(db.Servis, "Id", "Saat", gunServis.Servis_Id);
            return View(gunServis);
        }

        // GET: Üye/GunServis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GunServis gunServis = db.GunlerServis.Find(id);
            if (gunServis == null)
            {
                return HttpNotFound();
            }
            ViewBag.Gunler_Id = new SelectList(db.Gunler, "Id", "GunAd", gunServis.Gunler_Id);
            ViewBag.Servis_Id = new SelectList(db.Servis, "Id", "Id", gunServis.Servis_Id);
            return View(gunServis);
        }

        // POST: Üye/GunServis/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Gunler_Id,Servis_Id")] GunServis gunServis)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gunServis).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Gunler_Id = new SelectList(db.Gunler, "Id", "GunAd", gunServis.Gunler_Id);
            ViewBag.Servis_Id = new SelectList(db.Servis, "Id", "Id", gunServis.Servis_Id);
            return View(gunServis);
        }

        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
