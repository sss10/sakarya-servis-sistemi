﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SSS.Models;

namespace SSS.Controllers
{
    public class DuraksController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Duraks
        public IQueryable<Durak> GetDuraks()
        {
            return db.Duraks;
        }

        // GET: api/Duraks/5
        [ResponseType(typeof(Durak))]
        public IHttpActionResult GetDurak(int id)
        {
            Durak durak = db.Duraks.Find(id);
            if (durak == null)
            {
                return NotFound();
            }

            return Ok(durak);
        }

        // PUT: api/Duraks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDurak(int id, Durak durak)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != durak.Id)
            {
                return BadRequest();
            }

            db.Entry(durak).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DurakExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Duraks
        [ResponseType(typeof(Durak))]
        public IHttpActionResult PostDurak(Durak durak)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Duraks.Add(durak);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = durak.Id }, durak);
        }

        // DELETE: api/Duraks/5
        [ResponseType(typeof(Durak))]
        public IHttpActionResult DeleteDurak(int id)
        {
            Durak durak = db.Duraks.Find(id);
            if (durak == null)
            {
                return NotFound();
            }

            db.Duraks.Remove(durak);
            db.SaveChanges();

            return Ok(durak);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DurakExists(int id)
        {
            return db.Duraks.Count(e => e.Id == id) > 0;
        }
    }
}