﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sakarya_Servis_Sistemi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Hakkimizda()
        {
                return View(); 
        }

        public ActionResult Galeri()
        {
            return View();
        }
        public ActionResult OkulSecenekleri()
        {
            return View();
        }
        public ActionResult Personeller()
        {
            return View();
        }
        public ActionResult Aktiviteler()
        {
            return View();
        }
    }
}