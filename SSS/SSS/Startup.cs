﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SSS.Startup))]
namespace SSS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
