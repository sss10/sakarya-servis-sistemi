namespace SSS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Db2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Duraks", "Latitude", c => c.Double(nullable: false));
            DropColumn("dbo.Duraks", "Langitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Duraks", "Langitude", c => c.Double(nullable: false));
            DropColumn("dbo.Duraks", "Latitude");
        }
    }
}
