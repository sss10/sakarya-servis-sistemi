namespace SSS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Db3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sofors", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Sofors", new[] { "User_Id" });
            AddColumn("dbo.AspNetUsers", "Ogrenci_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Sofor_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Yonetici_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Ogrenci_Id");
            CreateIndex("dbo.AspNetUsers", "Sofor_Id");
            CreateIndex("dbo.AspNetUsers", "Yonetici_Id");
            AddForeignKey("dbo.AspNetUsers", "Ogrenci_Id", "dbo.Ogrencis", "Id");
            AddForeignKey("dbo.AspNetUsers", "Sofor_Id", "dbo.Sofors", "Id");
            AddForeignKey("dbo.AspNetUsers", "Yonetici_Id", "dbo.Yoneticis", "Id");
            DropColumn("dbo.Sofors", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sofors", "User_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.AspNetUsers", "Yonetici_Id", "dbo.Yoneticis");
            DropForeignKey("dbo.AspNetUsers", "Sofor_Id", "dbo.Sofors");
            DropForeignKey("dbo.AspNetUsers", "Ogrenci_Id", "dbo.Ogrencis");
            DropIndex("dbo.AspNetUsers", new[] { "Yonetici_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Sofor_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Ogrenci_Id" });
            DropColumn("dbo.AspNetUsers", "Yonetici_Id");
            DropColumn("dbo.AspNetUsers", "Sofor_Id");
            DropColumn("dbo.AspNetUsers", "Ogrenci_Id");
            CreateIndex("dbo.Sofors", "User_Id");
            AddForeignKey("dbo.Sofors", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
