namespace SSS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Db1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OgrenciGunlers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ogrenci_Id = c.Int(),
                        Gun_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gunlers", t => t.Gun_Id)
                .ForeignKey("dbo.Ogrencis", t => t.Ogrenci_Id)
                .Index(t => t.Ogrenci_Id)
                .Index(t => t.Gun_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OgrenciGunlers", "Ogrenci_Id", "dbo.Ogrencis");
            DropForeignKey("dbo.OgrenciGunlers", "Gun_Id", "dbo.Gunlers");
            DropIndex("dbo.OgrenciGunlers", new[] { "Gun_Id" });
            DropIndex("dbo.OgrenciGunlers", new[] { "Ogrenci_Id" });
            DropTable("dbo.OgrenciGunlers");
        }
    }
}
