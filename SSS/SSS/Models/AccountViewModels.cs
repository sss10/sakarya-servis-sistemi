﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSS.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Diesen Browser merken?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "E-Mail")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Sifre")]
        public string Password { get; set; }

        [Display(Name = "Beni hatirla?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} harften oluşmalı.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Ad")]
        public string Ad { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} harften oluşmalı.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Soyad")]
        public string Soyad { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} rakamdan oluşmalı.", MinimumLength = 10)]
        [DataType(DataType.Text)]
        [Display(Name = "Telefon")]
        public string Telefon { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} harften oluşmalı.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Okul Adi")]
        public string Okulad { get; set; }

        [Required]
        [Display(Name = "Okul Turu ID")]
        public int OkulTurID { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" en az {2} uzunluğunda olmalı.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Sifre")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Tekrarlayiniz")]
        [Compare("Password", ErrorMessage = "Girilen sifreler uyusmamaktadir.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "\"{0}\" muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Kennwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kennwort bestätigen")]
        [Compare("Password", ErrorMessage = "Das Kennwort stimmt nicht mit dem Bestätigungskennwort überein.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }
    }
}
