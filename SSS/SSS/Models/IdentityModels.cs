﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace SSS.Models
{
    // Sie können Profildaten für den Benutzer hinzufügen, indem Sie der ApplicationUser-Klasse weitere Eigenschaften hinzufügen. Weitere Informationen finden Sie unter https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        [ForeignKey("Ogrenci")]
        public int? Ogrenci_Id { get; set; }
        public virtual Ogrenci Ogrenci { get; set; }
        [ForeignKey("Sofor")]
        public int? Sofor_Id { get; set; }
        public virtual Sofor Sofor { get; set; }
        [ForeignKey("Yonetici")]
        public int? Yonetici_Id { get; set; }
        public virtual Yonetici Yonetici { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Beachten Sie, dass der "authenticationType" mit dem in "CookieAuthenticationOptions.AuthenticationType" definierten Typ übereinstimmen muss.
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Benutzerdefinierte Benutzeransprüche hier hinzufügen
            return userIdentity;
        }
    }
    public class Ogrenci
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Telefon { get; set; }
        [ForeignKey("Okul")]
        public int Okul_Id { get; set; }
        public virtual Okul Okul { get; set; }
        [ForeignKey("Durak")]
        public int? Durak_Id { get; set; }
        public virtual Durak Durak { get; set; }
    }
    public class Sofor
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        
    }
    public class Okul
    {
        public int Id { get; set; }
        public string OkulAdi { get; set; }
        [ForeignKey("OkulTur")]
        public int OkulTur_Id { get; set; }
        public virtual OkulTur OkulTur { get; set; }
    }
    public class OkulTur
    {
        public int Id { get; set; }
        public string Tur { get; set; }
    }

    public class Gunler
    {
        public int Id { get; set; }
        public string GunAd { get; set; }
        public bool Katilim { get; set; }

    }
    public class OgrenciGunler
    {
        public int Id { get; set; }
        [ForeignKey("Ogrenci")]
        public int? Ogrenci_Id { get; set; }
        public virtual Ogrenci Ogrenci { get; set; }
        [ForeignKey("Gunler")]
        public int? Gun_Id { get; set; }
        public virtual Gunler Gunler { get; set; }
    }
    public class Servis
    {
        public int Id { get; set; }
        public int Saat { get; set; }
        
    }
    public class GunServis
    {
        public int Id { get; set; }
        [ForeignKey("Gunler")]
        public int Gunler_Id { get; set; }
        public virtual Gunler Gunler { get; set; }
        [ForeignKey("Servis")]
        public int Servis_Id { get; set; }
        public virtual Servis Servis { get; set; }
    }
    public class Durak
    {
        public int Id { get; set; }
        public string Ilce { get; set; }
        public string Mahalle { get; set; }
        public string Sokak { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    public class Yonetici
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        
        //public System.Data.Entity.DbSet<SSS.Models.ApplicationUser> ApplicationUsers { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Durak> Duraks { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Ogrenci> Ogrencis { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Okul> Okuls { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.OkulTur> OkulTurs { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Servis> Servis { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Gunler> Gunler { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.GunServis> GunlerServis { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Sofor> Sofor { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.Yonetici> Yonetici { get; set; }
        public System.Data.Entity.DbSet<SSS.Models.OgrenciGunler> OgrenciGunler { get; set; }
    }
}